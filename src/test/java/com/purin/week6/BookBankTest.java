package com.purin.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void sholudwithdrawsuccess(){
        Bookbank book = new Bookbank("purin", 100);
        book.withdraw(50);
        assertEquals(50, book.getbalance(),0.000001);
    }
    @Test
    public void sholudwithdrawoversuccess(){
        Bookbank book = new Bookbank("purin", 100);
        book.withdraw(150);
        assertEquals(100, book.getbalance(),0.000001);
    }
    @Test
    public void sholudwithdrawwithnagativenumber(){
        Bookbank book = new Bookbank("purin",100);
        book.withdraw(-100);
        assertEquals(100, book.getbalance(),0.000001);
    }

    @Test
    public void sholuddepositsuccess(){
        Bookbank book = new Bookbank("purin", 100);
        book.deposit(300);
        assertEquals(400, book.getbalance(),0.000001);
    }

    @Test
    public void sholuddepositsuccess2(){
        Bookbank book = new Bookbank("purin", 100);
        book.deposit(800);
        assertEquals(900, book.getbalance(),0.000001);
    }
}
