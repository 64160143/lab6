package com.purin.week6;

import static org.junit.Assert.assertEquals;

import javax.naming.spi.DirStateFactory.Result;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownover(){
        Robot robot = new Robot("Robot",'R',0,Robot.MAX_X);
        assertEquals(false,robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldcreateNsuccess1() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6,robot.getY());
    }

    @Test
    public void shouldcreateNsuccess2() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0,robot.getY());
    }

    @Test
    public void shouldcreateNfail() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0,robot.getY());
    }

    @Test
    public void shouldcreateNfail2() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.down(12);
        assertEquals(false, result);
        assertEquals(0,robot.getY());
    }

    @Test
    public void shouldcreateNfail3() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0,robot.getX());
    }

    @Test
    public void shouldcreateNfail4() {
        Robot robot = new Robot("Robot",'R' ,10, 11);
        boolean result = robot.right(12);
        assertEquals(false, result);
        assertEquals(0,robot.getY());
    }


    @Test
    public void shouldcreatesuccess2() {
        Robot robot = new Robot("Robot",'R');
        assertEquals("Robot",robot.getName());
        assertEquals('R',robot.getsymbol());
        assertEquals(0,robot.getX());
        assertEquals(0,robot.getY());
    }
   
   
    @Test
    public void shouldupNegative(){
        Robot robot = new Robot("Robot",'R',0,Robot.MIN_Y);
        assertEquals(false,robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldDownSuccess(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true,robot.down());
        assertEquals(1,  robot.getY());
    }

    @Test
    public void shouldupSuccess(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(true,robot.up());
        assertEquals(0,  robot.getY());
    }

    @Test
    public void shouldrightSuccess(){
        Robot robot = new Robot("Robot",'R',0,3);
        assertEquals(true,robot.right());
        assertEquals(3,  robot.getY());
    }
    
    @Test
    public void shouldleftSuccess(){
        Robot robot = new Robot("Robot",'R',0,4);
        assertEquals(true,robot.left());
        assertEquals(4,  robot.getY());
    }
}
