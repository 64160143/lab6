package com.purin.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public Robot(String name,char symbol,int x,int y){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }


    public Robot(String name, char symbol){ //signature
        this(name , symbol, 0 , 0);
    }


    public void print(){
        System.out.println(name + "X: " + x + "y: " + y);
    }
    public boolean up(){
        if(y == MIN_Y) return false;
        this.y = this.y - 1;

        return true;
    }

    public boolean up(int step){
        for(int i=0; i<step; i++){
            if(!up()){
                return false;
            }    
        }
        return true;
    }

    public boolean down(){
        if(y == MAX_Y) return false;
        y = y + 1;
        return true;
    }
    
    public boolean down(int step){
        for(int i=0; i<step; i++){
            if(!down()){
                return false;
            }    
        }
        return true;
    }

    public boolean left(){
        x = x - 1;
        return true;
    }

    public boolean left(int step){
        for(int i=0; i<step; i++){
            if(!left()){
                return false;
            }    
        }
        return true;
    }

    public boolean right(){
        x = x + 1;
        return true;
    }

    public boolean right(int step){
        for(int i=0; i<step; i++){
            if(!right()){
                return false;
            }    
        }
        return true;
    }
   
    public void setName(String name){
        this.name = name;
    }
    public String getName(){  //Getter methods
        return name;
    }
    public char getsymbol(){
        return symbol;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

}
