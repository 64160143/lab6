package com.purin.week6;

public class Bookbank {
    // Attributte
    private String name;
    private double balance;
    //public bookbank
    public Bookbank(String name , double balance){//Cpnstruter
        this.name = name;
        this.balance = balance;
    }

    // Methods
    public boolean deposit(double money) {
        
        balance = balance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if(money<1) return false;
        if(money>balance) return false;
        balance = balance - money;
        return true;
    }
    public void print() {
        System.out.println(name + " " + balance);
    }

    public String getname(){
        return name;
    }

    public double getbalance(){
        return balance;
    }
}   

